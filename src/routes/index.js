import { Navigate } from "react-router";
import MainLayout from "../layouts/MainLayout";
import SignInLayout from "../layouts/SignInLayout";
import Home from "../pages/Home";
import SignIn from "../pages/SignIn";
import StuffList from "../pages/StuffList";
import Salem_StuffList from "../pages/Salem_StuffList";
import Ghavami_StuffList from "../pages/Ghavami/Ghavami_StuffList";
import RegisterUser from "../pages/RegisterUser";
import UserList from "../pages/UserList";
import Salem_RegisterUser from "../pages/Salem_RegisterUser";
import Salem_UserList from '../pages//Salem_UserList';
import Ghavami_RegisterUser from '../pages/Ghavami/RegisterUser';
import Ghavami_UserList from '../pages/Ghavami/UserList'


const routes = ({ isLogged = false }) => [
  {
    path: "/sign-in",
    element: <SignInLayout />,
    children: [
      {
        path: "",
        element: <SignIn />,
      },
    ],
  },
  {
    path: "/",
    // element: isLogged ? <MainLayout /> : <Navigate to="/sign-in" replace />,
    element: <MainLayout />,
    children: [
      {
        path: "",
        element: <Home />,
      },
      {
        path: "stuff-list",
        element: <StuffList/>,
      },
      {
          path: "salem-register-user",
              element: <Salem_RegisterUser/>,
      },
      {
        path: "ghavami-stuff-list",
        element: <Ghavami_StuffList/>,
      },
      {
        path: "register-user",
        element: <RegisterUser/>,
      },
      {
        path: "user-list",
        element: <UserList/>,
      },
      {
          path: "salem-stuff-list",
              element: <Salem_StuffList/>,
      },
      {
        path: "salem-user-list",
            element: <Salem_UserList/>,
      },
      {
        path: "ghavami-register-user",
        element: <Ghavami_RegisterUser/>,
      },
      {
        path: "ghavami-user-list",
        element: <Ghavami_UserList/>,
      },

    ],
  },
  {
    path: "*",
    element: <h1 className="text-center py-5">404</h1>,
  },
];
export default routes;
