const profile = {};
export default function reducer(state = profile, action) {
  switch (action.type) {
    case "SET_PROFILE":
      return { ...state, ...action.data };
    case "SET_PROFILE_IMAGE":
      return { ...state, image: action.data };
    case "REMOVE_PROFILE":
      return {};
    default:
      return state;
  }
}
