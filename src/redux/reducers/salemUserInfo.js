const salemUserInfo = {};
export default function reducer(state = salemUserInfo, action) {
  switch (action.type) {
    case "SET_SALEMUSERINFO":
        return { ...state, ...action.data };
    case "CLEAR_SALEMUSERINFO":
        return { };
    default:
        return state;
}
}
