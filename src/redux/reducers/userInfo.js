const userInfo = {};
export default function reducer(state = userInfo, action) {
    switch (action.type) {
        case "SET_USERINFO":
            return { ...state, ...action.data };
        case "CLEAR_USERINFO":
            return { };
        default:
            return state;
    }
}
