import { combineReducers, createStore } from "redux";
import loading from "./reducers/loading";
import isLogged from "./reducers/isLogged";
import profile from "./reducers/profile";
import userInfo from "./reducers/userInfo";
import salemUserInfo from "./reducers/salemUserInfo";
const rootReducer = combineReducers({
  loading,
  isLogged,
  profile,
  userInfo,
  salemUserInfo
});
const store = createStore(rootReducer);
export default store;
