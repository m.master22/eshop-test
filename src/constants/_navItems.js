const navItems = [
  {
    pathname: '/',
    icon: 'house-door',
  },
  {
    pathname: 'stuff-list',
    icon: 'envelope',
  },
  {
    pathname: 'salem-stuff-list',
    icon: 'envelope',
  },

  {
    pathname: 'ghavami-stuff-list',
    icon: 'envelope',
  },
  {
    pathname: 'register-user',
    icon: 'envelope',
  },
  {
    pathname: 'user-list',
    icon: 'envelope',
  },
  {
    pathname: 'salem-register-user',
    icon: 'envelope',
  },
  {
    pathname: 'salem-user-list',
    icon: 'envelope',
  },
  {
    pathname: 'ghavami-register-user',
    icon: 'envelope',
  },
  {
    pathname: 'ghavami-user-list',
    icon: 'envelope',
  },
].map((item, index) => ({ ...item, title: `nav-items.${index}`, id: index }));
export default navItems;
