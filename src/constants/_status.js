const status = {};

const topUp = [
  {
    id: "0",
  },
  {
    id: "1",
  },
  {
    id: "2",
  },
].map((item) => ({ ...item, name: `status.top-up.${item.id}` }));

status.topUp = topUp;

export default status;
