import React, { useEffect, useState } from 'react';
import {
  FilterSection,
  Pagination,
  Select,
  Table,
  Text,
} from '../../../components';
// import { paymentMethods, status } from '../../constants';
import { axios } from '../../../boot';

// import moment from 'moment';
import { t } from 'i18next';
// import { toast } from '../../methods';
import display from '../../../constants/_display';

export default function Ghavami_StuffList() {
  const [categories, setCategories] = useState([]);
  const [categoryChilds, setCategoryChilds] = useState([]);
  const [childCategorySelectedId, setChildCategorySelectedId] = useState();
  const [parentCategorySelectedId, setParentCategorySelectedId] = useState();

  const [headerList, setHeaderList] = useState([]);
  const [searchList, setSearchList] = useState([]);

  // const formControls = [
  //   {
  //     tag: Select,
  //     state: 'category_id',
  //     items: categories,
  //   },

  //   {
  //     tag: Select,
  //     state: 'category2_id',
  //     items: category2,
  //   },
  //   {
  //     tag: Select,
  //     state: 'list-id',
  //     items: headerList,
  //   },
  //   {
  //     tag: Select,
  //     state: 'searchList_id',
  //     items: searchList,
  //   },
  // ].map((item, index) => ({
  //   ...item,
  //   rules: [],
  //   label: t(`stuff-list.filter-inputs.${index}`),
  // }));

  const [data, setData] = useState([]);
  const [gridInfo, setGridInfo] = useState({
    filterData: {},
    activePage: 1,
    totalPages: 1,
  });

  const getData = () => {
    console.log('++++');

    console.log(childCategorySelectedId);
    const pageSize = 5;
    const url = '/RESTfullAPI/singletonWebService';

    // if we do'nt use formdata, we can use this object
    // const body = {
    //     serviceName:'Eshop_Stuff_Header_List',
    //     userToken:'',
    //     inputParams:JSON.stringify(
    //             {
    //                 "Stuff_Category_ID":169,
    //                 "PageNum":1,
    //                 "ListType":"mobile"
    //             })
    // };
    //
    //
    const body = new FormData();
    body.set('serviceName', 'Eshop_Stuff_Header_List');
    body.set('userToken', '');
    body.set(
      'inputParams',
      JSON.stringify({
        Stuff_Category_ID: childCategorySelectedId,
        PageNum: 1,
        ListType: 'mobile',
      })
    );

    axios.post(url, body).then(({ data }) => {
      if (data.IsSuccessful) {
        setData(data?.List1 ?? []);
        setGridInfo({ ...gridInfo, totalPages: data?.List1.length / pageSize });
      }
    });
  };

  const getCategory = (parentId) => {
    const url = '/RESTfullAPI/singletonWebService';

    const body = new FormData();
    body.set('serviceName', 'Eshop_Stuff_Category_List');
    body.set('userToken', '');
    body.set('inputParams', JSON.stringify({ Parent_ID: parentId }));

    axios.post(url, body).then(({ data }) => {
      if (data.IsSuccessful) {
        const lst2 = [];
        data.List1.forEach((item) => {
          lst2.push({ id: item.ID.toString(), name: item.Name });
        });
        if (parentId === undefined) setCategories(lst2);
        else setCategoryChilds(lst2);
        // console.log(lst2);
      }
    });
  };

  const getList = () => {
    const url = '/RESTfullAPI/singletonWebService';

    const body = new FormData();
    body.set('serviceName', 'Eshop_Stuff_Header_List');
    body.set('userToken', '');
    body.set(
      'inputParams',
      JSON.stringify({
        Stuff_Category_ID: 6,
        PageNum: 1,
        ListType: 'mobile',
      })
    );

    axios.post(url, body).then(({ data }) => {
      if (data.IsSuccessful) {
        console.log(data);
        const lst3 = [];
        data.List1.forEach((item) => {
          lst3.push({ id: item.GUID, name: item.Title });
        });
        setHeaderList(lst3);
      }
    });
  };

  const getSearchList = () => {
    const url = '/RESTfullAPI/singletonWebService';

    const body = new FormData();
    body.set('serviceName', 'Eshop_Stuff_Header_List');
    body.set('userToken', '');
    body.set(
      'inputParams',
      JSON.stringify({
        Stuff_Category_ID: -1,
        PageNum: 1,
        Search_Item: '',
        ListType: 'mobile',
      })
    );

    axios.post(url, body).then(({ data }) => {
      if (data.IsSuccessful) {
        console.log(data);
        const lst4 = [];
        data.List1.forEach((item) => {
          lst4.push({ id: item.GUID, name: item.Title });
        });
        setSearchList(lst4);
      }
    });
  };

  useEffect(getData, []);
  useEffect(getData, [childCategorySelectedId]);

  // useEffect(getCategories, []);
  // useEffect(() => getCategory(), []);
  useEffect(getCategory, []);
  // useEffect(getList, []);
  // useEffect(getSearchList, []);

  return (
    <div className="TopUpRequests">
      {React.createElement(Select, {
        items: categories,
        setValue: (val) => {
          getCategory(val);
          setParentCategorySelectedId(val);
        },
        value: parentCategorySelectedId,
      })}

      {React.createElement(Select, {
        items: categoryChilds,
        setValue: (val) => {
          setChildCategorySelectedId(val);
        },
        value: childCategorySelectedId,
      })}

      <Table className="layout">
        <thead>
          <tr>
            <th>
              <Text value="stuff-list.table-headers.0" />
            </th>
            <th>
              <Text value="stuff-list.table-headers.1" />
            </th>
            <th>
              <Text value="stuff-list.table-headers.2" />
            </th>
            <th>
              <Text value="stuff-list.table-headers.3" />
            </th>
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => (
            <tr key={index}>
              <td>{item.Title}</td>
              <td>{item.Display}</td>
              <td>{item.BasicSalePrice}</td>
              <td>{item.FinalSalePrice}</td>
            </tr>
          ))}
        </tbody>
      </Table>

      {/* <FilterSection
        formControls={formControls}
        title="stuff-list.filter-title"
        onSubmit={(e) => {
          console.log(e);
          gridInfo.activePage = 1;
          gridInfo.filterData = e;
          getData(e);

          //loged an empty object. why?
          // console.log(e);
        }}
        showFilterLabel
      >
        <Table className="layout">
          <thead>
            <tr>
              <th>
                <Text value="stuff-list.table-headers.0" />
              </th>
              <th>
                <Text value="stuff-list.table-headers.1" />
              </th>
              <th>
                <Text value="stuff-list.table-headers.2" />
              </th>
              <th>
                <Text value="stuff-list.table-headers.3" />
              </th>
            </tr>
          </thead>
          <tbody>
            {data.map((item, index) => (
              <tr key={index}>
                <td>{item.Title}</td>
                <td>{item.Display}</td>
                <td>{item.BasicSalePrice}</td>
                <td>{item.FinalSalePrice}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          activePage={gridInfo.activePage}
          setActivePage={(e) => {
            gridInfo.activePage = e;
            getData();
          }}
          totalPages={gridInfo.totalPages}
        />
      </FilterSection> */}
    </div>
  );
}
