import React, { useEffect, useState } from 'react';
import './index.scss';
import { Table } from '../../../components';
import { IsNullOrEmpty } from '../../../methods/jsStringHelper';
import { useNavigate } from 'react-router';
//برای ست کردن دیتا در استور ریداکس
import { useDispatch } from 'react-redux';

export default function Ghavami_UserList() {
  const [data, setData] = useState([]);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const getData = () => {
    if (!IsNullOrEmpty(window.localStorage.getItem('halehStorage')))
      setData(JSON.parse(window.localStorage.getItem('halehStorage')));
  };
  useEffect(getData, []);

  const onDelete = (id) => {
    // به شرط موحود بودن لیست > کشیدن بیرون لیست از لوکال استورج
    if (!IsNullOrEmpty(window.localStorage.getItem('halehStorage'))) {
      const list = JSON.parse(window.localStorage.getItem('halehStorage'));
      // پیدا کردن ایندکس آبجکت با آیدی خاص
      const getObjectId = (object) => {
        return object.id === id;
      };
      const index = list.findIndex(getObjectId);
      //حذف کردن کامل یک آیتم از آرایه بر اساس ایندکس
      list.splice(index, 1);
      //ست کردن لوکال استورج با دیتای جدید
      window.localStorage.setItem('halehStorage', JSON.stringify(list));
      //آپدیت دیتا
      getData();
    }
  };

  const setUserInfo = (obj) => {
    const data = obj;
    dispatch({ type: 'SET_USERINFO', data });
  };

  const onEdit = (obj) => {
    navigate('/ghavami-register-user');
    setUserInfo(obj);
  };

  return (
    <div className="UserList mt-5">
      <Table className="layout">
        <thead>
          <tr>
            <th>نام</th>
            <th>نام خانوادگی</th>
            <th>کد ملی</th>
            <th>شماره همراه</th>
            <th>شناسه</th>
            <th>حذف</th>
            <th>ویرایش</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => (
            <tr key={index}>
              <td>{item.name}</td>
              <td>{item.family}</td>
              <td>{item.meliCode}</td>
              <td>{item.mobile}</td>
              <td>
                <p
                  style={{ width: '120px', margin: '0 auto' }}
                  className=" text-truncate mx-auto "
                >
                  {item.id}
                </p>
              </td>

              <td>
                <i
                  className="bi bi-file-earmark-minus ms-3 cursor-pointer fs-4"
                  onClick={() => onDelete(item.id)}
                ></i>
              </td>
              <td>
                <i
                  className="bi bi-pencil-square  cursor-pointer fs-4 "
                  onClick={() => onEdit(item)}
                ></i>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}
