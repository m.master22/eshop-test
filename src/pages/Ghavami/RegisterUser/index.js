import React, { useEffect, useState } from 'react';
import { Button, Form, Input } from '../../../components';
import './index.scss';
import { Col, Row } from 'react-bootstrap';
import { IsNullOrEmpty } from '../../../methods/jsStringHelper';
import { v4 as uuidv4 } from 'uuid';
import { rules } from '../../../constants';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';

export default function Ghavami_RegisterUser() {
  const [data, setData] = useState({});
  const navigate = useNavigate();
  const dispatch = useDispatch();
  //با دیسپچ اطلاعات را در ریداکس ذخیره میکنیم و برای خوندن از یوزسلکتور استفاده میکنیم
  const userInfo = useSelector((s) => s.userInfo); //با تغییر استدیت هرکجا با این هوک براش سابسکرایب تعریف شده باشه اتومات کال میکنند
  const emptyUserInfo = () => {
    const data = {};
    dispatch({ type: 'CLEAR_USERINFO', data });
  };

  const formControls = [
    {
      title: 'نام',
      state: 'name',
      rules: rules.required,
    },
    {
      title: 'نام خانوادگی',
      state: 'family',
      rules: rules.required,
    },
    {
      title: 'کد ملی',
      state: 'meliCode',
      rules: rules.required,
    },
    {
      title: 'شماره همراه',
      state: 'mobile',
      rules: rules.iranMobile,
    },
  ];

  const handelUpdate = () => {
    if (userInfo.id) {
      setData(userInfo);
    }
  };
  useEffect(handelUpdate, []);

  const handelSubmit = () => {
    let list;
    if (IsNullOrEmpty(window.localStorage.getItem('halehStorage'))) list = [];
    else list = JSON.parse(window.localStorage.getItem('halehStorage'));
    if (userInfo.id) {
      //برای آپدیت یوزر موجود که آیدی از قبل دارد {
      const index = list.findIndex((object) => {
        return object.id === userInfo.id;
      });
      list[index] = data; //  آبجکت با آیدی مورد نظر را در لیست به عنوان دیتا معرفی میکنیم
      emptyUserInfo();
      navigate('/ghavami-user-list');
    } else {
      //یا ایجاد کاربر جدید که آیدی ندارد

      data.id = uuidv4().toString();
      list.push(data);
    }
    window.localStorage.setItem('halehStorage', JSON.stringify(list));
    setData({});
  };

  const handelSubmitNew = () => {
    let list;
    if (IsNullOrEmpty(window.localStorage.getItem('halehStorage'))) list = [];
    else list = JSON.parse(window.localStorage.getItem('halehStorage'));
    data.id = uuidv4().toString();
    list.push(data);

    window.localStorage.setItem('halehStorage', JSON.stringify(list));
    emptyUserInfo();
    setData({});
  };

  return (
    <div className="RegisterUser">
      {/* چرا این باتن را نمیشه تو فرم بیارم؟ */}
      {/* <Button className="col-4" type="submit" onClick={handelSubmitNew}>
        افزودن کاربر جدید
      </Button> */}
      <div className="ps-3">
        <i
          className="bi bi-person-plus-fill cursor-pointer fs-4 ms-10"
          onClick={handelSubmitNew}
        ></i>
      </div>

      <Form className="row right-direction" onSubmit={handelSubmit}>
        <h5 className="col-12">ثبت اطلاعات کاربر</h5>
        {/* اگر نخوام از لوپ استفاده کنم برای آپدیت کردن ولیو ها باید 4 بار این را بنویسم و استیتها را عوض کنم */}
        {/* {React.createElement(Input, {
          value: data['name'],
          setValue: (val) => setData((data) => ({ ...data, name: val })),
        })} */}
        {formControls.map((item, index) => (
          <Col key={index} xs="12">
            <Row className="row-gap-1">
              <Col xs="6" md="3" className="text-start fs-7 px-3 px-md-0">
                {item.title}
                <span className="text-danger">
                  {item.rules?.length > 0 && '*'}
                </span>
              </Col>
              <Col xs="6" md="9">
                {React.createElement(Input, {
                  ...item, //به جای این میتوانستیم بنویسیم:status:item.status, rules:item.rules .....که آیتم رو توی لوپ گفتیم بر اساس آبچکتهای آرایه فرم کنترل درست کنه.یعنی میگیم پراپرتی هایی که اینپوت میگیره که یک آبجکت هست را برو از تو آیتم کپی کن
                  value: data[item.state], //چون ولیو رو داریم از چندین اینپوت میگیریم که اسم آنها را در استیت که همون لیبل است ذخیره کردیم
                  setValue: (
                    val //ولیو ها رو بایند کردیم
                  ) => setData((p) => ({ ...p, [item.state]: val })), // یعنی وقتی ولیو رو میخواهی ست کنی بیا برای دیتای ما یک پراپرتی درست کن . در ضمن فیلدهای قبلی پراپرتی رو نگه دار و کپی کن
                })}
              </Col>
            </Row>
          </Col>
        ))}

        <Col xs="12" className="row justify-content-end col-gap-2 px-4">
          <Button className="col-4" type="submit">
            ارسال
          </Button>
        </Col>
      </Form>
    </div>
  );
}
