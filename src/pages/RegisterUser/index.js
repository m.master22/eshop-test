import React, { useState, useEffect } from 'react';
import { Col, Row } from 'react-bootstrap';
import { v4 as uuidv4 } from 'uuid';
import './index.scss';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';

import { cloneDeep } from 'lodash';
import {
  Button,
  Form,
  Input,
  Modal,
  Select,
  Dropdown,
  Text,
} from '../../components';
import { rules } from '../../constants';
import { axios } from '../../boot';
import { getWebTemplate } from '../../methods';
import { t } from 'i18next';
import { IsNullOrEmpty } from '../../methods/jsStringHelper';

export default function RegisterUser({ show = true, onHide = () => {} }) {
  const [data, setData] = useState({});
  const dispatch = useDispatch();
  const userInfo = useSelector((s) => s.userInfo);
  const navigate = useNavigate();

  const emptyUserInfo = () => {
    const data = {};
    dispatch({ type: 'CLEAR_USERINFO', data });
  };

  const formControls = [
    {
      title: 'نام',
      state: 'name',
      rules: rules.required,
    },
    {
      title: 'نام خانوادگی',
      state: 'family',
      rules: rules.required,
    },
    {
      title: 'کد ملی',
      state: 'meliCode',
      rules: rules.required,
    },
    {
      title: 'شماره همراه',
      state: 'mobile',
      rules: rules.iranMobile,
    },
  ];

  const submitAccountInfo = () => {
    let list;
    if (IsNullOrEmpty(window.localStorage.getItem('myStorage'))) list = [];
    else list = JSON.parse(window.localStorage.getItem('myStorage'));

    if (userInfo.id) {
      // init form for update
      const index = list.findIndex((object) => {
        // index in list
        return object.id === userInfo.id;
      });
      list[index] = data;
      emptyUserInfo();
      navigate('/user-list');
    } else {
      // init form for new item
      data.id = uuidv4().toString();
      list.push(data);
    }

    window.localStorage.setItem('myStorage', JSON.stringify(list));

    setData({});
  };

  const check4Update = () => {
    if (userInfo.id) {
      setData(userInfo);
    }
  };

  useEffect(check4Update, []);

  return (
    <div className="RegisterUser">
      <Form className="row right-direction" onSubmit={submitAccountInfo}>
        <h5 className="col-12 text-start">header Register user</h5>

        {formControls.map((item, index) => (
          <Col key={index} xs="12">
            <Row className="row-gap-1">
              <Col xs="6" md="3" className="text-start fs-7 px-3 px-md-0">
                {item.title}
                <span className="text-danger">
                  {item.rules?.length > 0 && '*'}
                </span>
              </Col>
              <Col xs="6" md="9">
                {React.createElement(item.tag ?? Input, {
                  ...item,
                  value: data[item.state],
                  setValue: (val) =>
                    setData((p) => ({ ...p, [item.state]: val })),
                })}
              </Col>
            </Row>
          </Col>
        ))}

        <Col xs="12" className="row justify-content-end col-gap-2 px-4">
          <Button className="col-4" type="submit">
            Submit
          </Button>
        </Col>
      </Form>
    </div>
  );
}
