import React, { useEffect, useState } from "react";
import "./index.scss";
import {
    Table,
    Button
} from "../../components";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";

import {IsNullOrEmpty} from "../../methods/jsStringHelper";

export default function UserList() {

    const [data, setData] = useState([]);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const setUserInfo = (obj) => {
        const data = obj;
        dispatch({ type: "SET_USERINFO", data });
    };

    const getData = () => {
        if(!IsNullOrEmpty(window.localStorage.getItem("myStorage")))
             setData(JSON.parse(window.localStorage.getItem("myStorage")));
    };

    useEffect(getData, []);

    const onEdit = (obj) => {
        setUserInfo(obj);
        navigate('/register-user');
    }

    function onDelete(id)
    {
        if(!IsNullOrEmpty(window.localStorage.getItem("myStorage"))){
            const list = JSON.parse(window.localStorage.getItem("myStorage"));
            const index = list.findIndex(object => {
                return object.id === id;
            });
            list.splice(index , 1);

            window.localStorage.setItem("myStorage", JSON.stringify(list));
            getData();
        }
    }

    return (
        <div className="UserList mt-5">

                <Table className="layout">
                    <thead>
                    <tr>
                        <th>
                            نام
                        </th>
                        <th>
                            نام خانوادگی
                        </th>
                        <th>
                            کد ملی
                        </th>
                        <th>
                            شماره همراه
                        </th>
                        <th>
                            شناسه
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    {data.map((item, index) => (
                        <tr key={index}>

                            <td>{item.name}</td>
                            <td>{item.family}</td>
                            <td>{item.meliCode}</td>
                            <td>{item.mobile}</td>
                            <td>

                                <p
                                    style={{ width: "120px",margin:"0 auto" }}
                                    className="text-truncate mx-auto"
                                >
                                    {item.id}
                                </p>
                            </td>
                            <td>
                                <i className="bi bi-file-earmark-minus ms-3 cursor-pointer fs-4" onClick={()=>onDelete(item.id)}></i>
                                <i className="bi bi-pencil-square  cursor-pointer fs-4 " onClick={()=>onEdit(item)}></i>

                            </td>

                        </tr>
                    ))}
                    </tbody>
                </Table>


        </div>
    );
}