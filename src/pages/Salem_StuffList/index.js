import React, { useEffect, useState } from "react";
import {
    FilterSection,
    Pagination,
    Select,
    Table,
    Text,
} from "../../components";
import { paymentMethods, status } from "../../constants";
import { axios } from "../../boot";

import moment from "moment";
import {t} from "i18next";
import {toast} from "../../methods";
import display from "../../constants/_display";

export default function Salem_StuffList() {
    const formControls = [

        {
            tag: Select,
            state: "display",
            items: display.Display,
        },

        {
            tag: Select,
            state: "display",
            items: display.Display,
        },
        {
            tag: Select,
            state: "display",
            items: display.Display,
        },
        {
            tag: Select,
            state: "display",
            items: display.Display,
        },
    ].map((item, index) => ({
        ...item,
        rules: [],
        label: t(`stuff-list.filter-inputs.${index}`),
    }));

    const [data, setData] = useState([]);
    const [gridInfo, setGridInfo] = useState({filterData: {}, activePage: 1, totalPages: 1});



    const getData = () => {

        const pageSize = 5;
        const url = "";
        const body = new FormData();
        body.set('serviceName', 'Eshop_Stuff_Category_List');
        body.set('userToken', '');
        body.set('inputParams', JSON.stringify(
            {
                "Parent_ID":'168'
            }));


        axios.post(url, body).then(({data}) => {
            if(data.IsSuccessful){
                setData(data?.List1 ?? []);
                // setGridInfo({...gridInfo, totalPages: data?.List1.length / pageSize})
            }
        });
    };

    useEffect(getData, []);


    return (
        <div className="TopUpRequests">

            <FilterSection
                formControls={formControls}
                onSubmit={(e) => {
                    gridInfo.activePage = 1;
                    gridInfo.filterData = e;
                    getData(e)
                }}
                showFilterLabel
            >
                <Table className="layout">
                    <thead>
                    <tr>
                        <th>
                            Name
                        </th>
                        <th>
                            ID
                        </th>


                    </tr>
                    </thead>
                    <tbody>
                    {data.map((item, index) => (
                        <tr key={index}>

                            <td>{item.ID}</td>
                            <td>{item.Name}</td>


                        </tr>
                    ))}
                    </tbody>
                </Table>
                <Pagination
                    activePage={gridInfo.activePage}
                    setActivePage={(e) => {
                        gridInfo.activePage = e;
                        getData()
                    }}
                    totalPages={gridInfo.totalPages}
                />
            </FilterSection>

        </div>
    );
}