import React, { useEffect, useState } from 'react';
import {
  FilterSection,
  Pagination,
  Select,
  Table,
  Text,
} from '../../components';
import { paymentMethods, status } from '../../constants';
import { axios } from '../../boot';

import moment from 'moment';
import { t } from 'i18next';
import { toast } from '../../methods';
import display from '../../constants/_display';

export default function StuffList() {
  const [category, setCategory] = useState([]);

  const formControls = [
    {
      tag: Select,
      state: 'category',
      items: category,
    },

    {
      state: 'Item',
    },
    {
      tag: Select,
      state: 'display',
      items: display.Display,
    },
    {
      tag: Select,
      state: 'display',
      items: display.Display,
    },
  ].map((item, index) => ({
    ...item,
    rules: [],
    label: t(`stuff-list.filter-inputs.${index}`),
  }));

  const [data, setData] = useState([]);

  const [gridInfo, setGridInfo] = useState({
    filterData: {},
    activePage: 1,
    totalPages: 1,
  });

  const getData = (e) => {
    const pageSize = 5;
    const url = '/RESTfullAPI/singletonWebService';
    const body = new FormData();
    body.set('serviceName', 'Eshop_Stuff_Header_List');
    body.set('userToken', '');
    body.set(
      'inputParams',
      JSON.stringify({
        Stuff_Category_ID: e?.category ?? '-1',
        PageNum: 1,
        ListType: 'mobile',
      })
    );

    axios.post(url, body).then(({ data }) => {
      if (data.IsSuccessful) {
        setData(data?.List1 ?? []);
        // setGridInfo({...gridInfo, totalPages: data?.List1.length / pageSize})
      }
    });
  };

  const getCategories = () => {
    const url = '';
    const body = new FormData();
    body.set('serviceName', 'Eshop_Stuff_Category_List');
    body.set('userToken', '');
    body.set('inputParams', '');

    axios.post(url, body).then(({ data }) => {
      if (data.IsSuccessful) {
        const lst = [];
        data.List1.forEach((x) => {
          lst.push({ id: x.ID, name: x.Name });
        });
        setCategory(lst);
      }
    });
  };

  useEffect(getData, []);
  useEffect(getCategories, []);

  return (
    <div className="TopUpRequests">
      <FilterSection
        formControls={formControls}
        title="stuff-list.filter-title"
        onSubmit={(e) => {
          gridInfo.activePage = 1;
          gridInfo.filterData = e;
          getData(e);
        }}
        showFilterLabel
      >
        <Table className="layout">
          <thead>
            <tr>
              <th>
                <Text value="stuff-list.table-headers.0" />
              </th>
              <th>
                <Text value="stuff-list.table-headers.1" />
              </th>
              <th>
                <Text value="stuff-list.table-headers.2" />
              </th>
              <th>
                <Text value="stuff-list.table-headers.3" />
              </th>
            </tr>
          </thead>
          <tbody>
            {data.map((item, index) => (
              <tr key={index}>
                <td>{item.Title}</td>
                <td>{item.Display}</td>
                <td>{item.BasicSalePrice}</td>
                <td>{item.FinalSalePrice}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Pagination
          activePage={gridInfo.activePage}
          setActivePage={(e) => {
            gridInfo.activePage = e;
            getData();
          }}
          totalPages={gridInfo.totalPages}
        />
      </FilterSection>
    </div>
  );
}
