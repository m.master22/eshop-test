import React, { useEffect, useState,useId} from "react";
import { Col, Row } from "react-bootstrap";
import { v4 as uuidv4 } from 'uuid'
import "./index.scss";
import {
    Form,
    FilterSection,
    Pagination,
    Select,
    Table,
    Text,
    Input,
    Button
} from "../../components";
import { rules } from "../../constants";
import {IsNullOrEmpty} from "../../methods/jsStringHelper";

import { useSelector,useDispatch } from 'react-redux';
import { calendarFormat } from "moment-jalaali";


export default function Salem_RegisterUser() {
    const [data, setData] = useState({});
    const salemUserInfo = useSelector((s) => s.salemUserInfo);
    const dispatch= useDispatch();
    const unique_id = uuidv4().toString();
    const formControls = [
 
         {
            title: "نام",
            state: "name",
            rules: rules.required,
        },

        {
            title: "نام خانوادگی",
            state: "family",
            rules: rules.required,
        },
        {
            title: "کد ملی ",
            state: "nationalCode",
            rules: rules.required,
            
        },
        {
            title: "شماره همراه",
            state: "mobileNumber",
            rules: rules.required,
        }];

        const submitAccountInfo = () => {
            let list;
            if(IsNullOrEmpty(window.localStorage.getItem("myStorage")))
                list = [];
            else
                list = JSON.parse(window.localStorage.getItem("myStorage"));
            console.log(list);
            data.id =unique_id;
            list.push(data);
            window.localStorage.setItem("myStorage", JSON.stringify(list));
            setData({});
            console.log("111111111111111");
            console.log(list);
            // setUserInfo(list);
       
        }

        // const setUserInfo = (data) => {
        //     console.log("lklklklskdlksldkslkdlsk");
        //     console.log(data);
        //      dispatch({ type: "SET_SALEMUSERINFO", data });
             
            
        //   };
const addNewItems =(data)   =>{
    clearForm();
    console.log("newItems");
    let list;
            if(IsNullOrEmpty(window.localStorage.getItem("myStorage")))
            list = [];
        else
            list = JSON.parse(window.localStorage.getItem("myStorage"));
        console.log(list);
        data.id =unique_id;
        list.push(data);
        // console.log(data);


        window.localStorage.setItem("myStorage", JSON.stringify(list));

        setData({});

};
const clearForm= () =>{
    const data ={};
    dispatch({ type: "CLEAR_SALEMUSERINFO", data });
};

const editItems =(data)   =>{
    console.log("editItems");
};
        
const checkForm = () =>{
    if (salemUserInfo.id){
        setData(salemUserInfo);
    }
  };
  useEffect(checkForm, []);

    return (
        <div className='SalemRegisterUser'>
         <Form className="row right-direction" onSubmit={submitAccountInfo}  >
            <h5 className="col-12 text-start">
                "فرم ثبت نام کاربران"
            </h5>

                    {formControls.map((item, index) => (
                    <Col key={index} xs="12">
                        <Row className="row-gap-1">
                        <Col xs="6" md="3" className="text-start fs-7 px-3 px-md-0">
                        {item.title}
                    <span className="text-danger">
                        {item.rules?.length > 0 && "*"}
                    </span>
                    </Col>
                    <Col xs="6" md="9">
                        {React.createElement(item.tag ?? Input, {
                                ...item,
                            value: data[item.state],
                        setValue: (val) =>
                        setData((p) => ({ ...p, [item.state]: val })),
                    })}
                    </Col>
                    </Row>
                    </Col>
                    ))}

        <Col xs="12" className="row justify-content-end col-gap-2 px-4">
                <Button className="col-4" type="submit">
                Submit
                </Button>
                <Button className="col-4" type="addNew" onClick={addNewItems}>
                addNew
                </Button>
                <Button className="col-4" type="edit" onClick={editItems}>
                edit
                </Button>
                </Col>
        </Form>




        </div>
    );
}