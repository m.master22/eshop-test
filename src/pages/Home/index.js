import { Col, Row } from "react-bootstrap";
import "./index.scss";
import {axios} from "../../boot";
import {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import Chart from 'chart.js/auto';
import { Line } from 'react-chartjs-2';
import {useDispatch} from "react-redux";
import {useNavigate} from "react-router";

export default function Home() {
  const todoItems = [
    {
      title: "Personal",
      color: "info",
      bodyText: "Rework Back-End API Collection",
    },
    {
      title: "Done",
      color: "success",
      bodyText: "Doctors Appointment",
    },
    {
      title: "In proccess",
      color: "warning",
      bodyText: "Setup the Front-End Framework",
    },
  ];
  const [data, setData] = useState({});
  const [chartData, setChartData] = useState({});
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const chartConfig = {
    labels: chartData.chart_labels,
    datasets: [
      {
        label: "Top Up amounts",
        data: chartData.chart_value,
        fill: true,
        backgroundColor: "rgba(75,192,192,0.2)",
        borderColor: "rgba(75,192,192,1)"
      },
    ]
  };
  
    const getChartData = () => {
    const url = "/v1/web_template/getWeb_Admin_MainChart";
    const body = "";
    axios.post(url, body).then(({ data }) => {
      const list = data?.data ?? {};
      const chart_labels = list.map(function(item){return item.chart_label});
      const chart_value = list.map(function(item){return item.chart_value});
      setChartData({chart_labels,chart_value})
    });
  }
  
  const getData = () => {
    const url = "/v1/web_template/getWeb_Admin_Template_Info";
    const body = "";
    axios.post(url, body).then(({ data }) => {
      setData(data?.data ?? {});
    });
  };

  const login_fake = () => {
   // dispatch({ type: "SET_IS_LOGGED", data });
    //navigate("/");
  }

  useEffect(()=> {
 //   getData();
  //  getChartData();
  },[])

  return (
    <Row className="Home align-items-start">

      <Col xs="12" lg="6">
        <div className="chart-section bg-white p-3 rounded shadow-sm">
          <div className="fs-1 w-100 h-100 border border-secondary rounded d-flex flex-center">
             <Line data={chartConfig} />
          </div>
        </div>
      </Col>
      <Col xs="12" lg="1" xl="2"></Col>
      <Col xs="12" lg="5" xl="4">
        <div className="calendar-section fs-1 text-white d-flex flex-center rounded shadow-sm bg-dark bg-opacity-50">
          calendar
        </div>
      </Col>
      <Col xs="12" lg="6">
        <h4>You have:</h4>
        <hr className="bg-dark my-2" />
        <ul className="notify-section">
          <li>
            <p className='position-relative'>
              <span className="d-inline-block text-danger fs-2 fw-bold text-center">
                {data.compliance_Count}
              </span>
              <Link
                  to="/compliance-check"
                  className="position-absolute top-50 translate-middle-y"
              >
                Pending compliance requests
              </Link>
            </p>
          </li>
          <li>
            <p className='position-relative'>
              <span className="d-inline-block text-danger fs-2 fw-bold text-center">
                {data.ad_Account_Count}
              </span>
              <Link
                  to="/ad-account-request"
                  className="position-absolute top-50 translate-middle-y"
              >
                Pending ad account requests
              </Link>

            </p>
          </li>
          <li>
            <p className='position-relative'>
              <span className="d-inline-block text-danger fs-2 fw-bold text-center">
                {data.top_Up_Count}
              </span>
              <Link
                  to="/top-up-requests"
                  className="position-absolute top-50 translate-middle-y"
              >
                Pending Top up requests
              </Link>

            </p>
          </li>
        </ul>
      </Col>
      <Col xs="12" lg="1" xl="2"></Col>
      <Col xs="12" lg="5" xl="4">
        <h4>Today 27 January 2022</h4>
        <hr className="bg-dark my-2" />
        {todoItems.map((item, index) => (
          <div key={index} className="todo-item w-100 my-1">
            <h5 className="title text-secondary">{item.title}</h5>
            <div className="d-flex flex-center col-gap-2 border border-secondary rounded">
              <span className={`progress rounded h-100 bg-${item.color}`} />
              <div className="body h-100 d-flex flex-column justify-content-center">
                <h6>{item.bodyText}</h6>
                <p>{item.title}</p>
              </div>
            </div>
          </div>
        ))}
      </Col>
    </Row>
  );
}
