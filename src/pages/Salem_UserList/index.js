import React, { useEffect, useState } from "react";
import {
    Table,
} from "../../components";
import "./index.scss";
import {IsNullOrEmpty} from "../../methods/jsStringHelper";
import Button from './../../components/Button/index';
import { useNavigate } from 'react-router';
import Salem_RegisterUser from './../Salem_RegisterUser/index';
import { useSelector,useDispatch } from 'react-redux';

export default function Salem_UserList() {
    const [data, setData] = useState([]);
    const navigate=useNavigate();
    const dispatch= useDispatch();

    const getData = (e) => {
        if(!IsNullOrEmpty(window.localStorage.getItem("myStorage")))
             setData(JSON.parse(window.localStorage.getItem("myStorage")));
    };


const onEdit = (e)=>{
    setUserInfo(e);
    navigate('/salem-register-user');
}    
  const onDelete =(e) =>{
    console.log(e);
    if(!IsNullOrEmpty(window.localStorage.getItem("myStorage"))){
        var list= JSON.parse(window.localStorage.getItem("myStorage"));
        var t =list.findIndex(object => object.id === e);
        list.splice(t,1);
        window.localStorage.setItem("myStorage", JSON.stringify(list));
        console.log(list);
        getData();

    }
    
  };
  const setUserInfo = (data) => {
     dispatch({ type: "SET_SALEMUSERINFO", data });
  };
  useEffect(getData, []);

    return (
        <div className="SalemUserList">

                <Table className="layout">
                    <thead>
                    <tr>
                        <th>
                            نام
                        </th>
                        <th>
                            نام خانوادگی
                        </th>
                        <th>
                            کد ملی
                        </th>
                        <th>
                            شماره همراه
                        </th>
                        <th>
                            عملیات
                        </th>
                        

                    </tr>
                    </thead>
                    <tbody>
                    {data.map((item, index) => (
                        <tr key={index}>

                            <td>{item.name}</td>
                            <td>{item.family}</td>
                            <td>{item.nationalCode}</td>
                            <td>{item.mobileNumber}</td>
                            
                            <td> <i className="bi bi-file-earmark-minus me-3 cursor-pointer" onClick={()=>onDelete(item.id)}></i>
                                     <i className="bi bi-pencil-square"  onClick={() => onEdit(item)}></i>

                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>


        </div>
    );
}